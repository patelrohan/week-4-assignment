// const request = require('supertest');
// //const abc = require('./rest-app');

// const expect = require('chai').expect;

// // /* describe('[GET /show], [Post /send], [Post /update], [Delete /remove]', () => {
// //     it('Get all the user from DB', (done) => {
// //         request('http://localhost:5000')
// //         .get('/show')
// //         .set('Accept', 'application/json')
// //         .expect(200, done)
// //     });
    
// //     // 

// //     // it('Updating the data to DB', (done) => {
// //     //     request('http://localhost:5000')
// //     //     .post('/update')
// //     //     .set('send',{
// //     //         "CUSTID": 12,
// //     //         "CUSTOMERNAME": "shubham",
// //     //         /* "ADDRESS": "Mumbai",
// //     //         "CITY": "mumbai",
// //     //         "POSTALCODE": 221406 */
// //     //     })
// //     //     .expect(200, done)
// //     // });

// //     // it('Deleting the data from DB', (done) => {
// //     //     request('http://localhost:5000')
// //     //     .post('/remove')
// //     //     .set('send',{
// //     //         // "CUSTID": 12,
// //     //         // "CUSTOMERNAME": "shubham",
// //     //         /* "ADDRESS": "Mumbai",
// //     //         "CITY": "mumbai",
// //     //         "POSTALCODE": 221406 */
// //     //     })
// //     //     .expect(200, done)
// //     // });
// // }); */
               
// describe('[GET /show], [Post /send], [Post /update], [Delete /remove]', () => {
// it('Posting the data to DB', (done) => {
//         request('http://localhost:5000')
//         .post('/send')
//         .set('Accept','application/json')
//         .send({
//             "CUSTID": 12,
//             "CUSTOMERNAME": "shubham",
//             "ADDRESS": "Mumbai",
//             "CITY": "mumbai",
//             "POSTALCODE": 221406
//         })
//         // .expect(200, done)
//         .then((res) => {
//             const body = res.body
//             console.log(res)
//             // expect(body).to.have.keys('class', 'marks', 'name', 'subject')
//             // expect(body).to.have.property('name')
//             // expect(body.name).to.be.a('string')
//             done();
//           })
//           .catch((err) => {
//             console.log(err.message)
//             done()
//           })
//     });
// });



const request = require("supertest");
const chai = require("chai")
const expect = chai.expect;
// var should = require('chai').Should()

const app = "http://localhost:5000"
describe('GET /show', () => {
  it("Respond with all the users", (done) => {
    request(app)
      .get('/show')
      .set('Accept', 'application/json')
      .expect(200, done)
  })
});

describe('[GET /show], [Post /send], [Post /update], [Delete /remove]', () => {
  it('Posting the data to DB', (done) => {
          request('http://localhost:5000')
          .post('/send')
          .set('Accept', 'application/json')
          .send({
                        "CUSTID": 12,
                        "CUSTOMERNAME": "shubham",
                        "ADDRESS": "Mumbai",
                        "CITY": "mumbai",
                        "POSTALCODE": 221406
                    })
          .expect(200, done)
      });
  });

describe('POST /send', () => {
  it('Should Post new Data', (done) => {
    request(app)
      .post('/send')
      .set('Accept', 'application/json')
      .send({
       "name": 'Ronald',
        "class": "gryffindor",
        "subject": "Astronomy",
        "marks": 100,
        "id": 102
      })
      .expect(200,done)  
      .then((res) => {
        const body = res.body
        // console.log(typeof body.name)
        expect(body).to.have.keys('class', 'marks', 'name', 'subject')
        expect(body).to.have.property('name')
        // expect(body.name).to.be.a('string')
        done();
      })
      .catch((err) => {
        console.log(err.message)
        done()
      }) 
  })
})

/* describe('POST /update', () => {
  it("Should Update successfully", (done) => {
    request(app)
      .post('/update')
      .send({
        "id": 5454,
        "name": "dfjkdfk"
      })
      .then((res) => {
        const body = res.body
        console.log(body)
        expect(200)
        done()
      })
  })
}) */

/* describe('DELETE /remove', ()=>{
  it("Should Delete successfully",(done)=>{
    request(app)
    .post('/remove')
    .send({
      "name":"54454"
    })
    .then((res)=>{
      expect(200)
      done()
    })
  })
}) */