const request = require('supertest');
const chai = require('chai');
const expect = chai.expect;

const apprest = "http://localhost:5000"
describe('Get/ send', () => {
    it('get all data from db', (done) => {
        request(apprest)
        .get('/show')
        .set('Accept', 'Application.json')
        // .expect(200,done)
        .then((res) => { 
            const body = res.body
            console.log(res);
            done();
        })
        .catch((err) => { 
            console.log(err.message)
            done();
        })
    })
});

describe(' Post /send', () => {
    it('Post the new Data to db', (done) => {
        request(apprest)
        .post('/send')
        .set('Accept', 'Application/json') 
        .send({
                "CustId": 12,
                "CustomerName": "Mick"
        })
        .then((res) => { 
            const body = res.body
            console.log(res);
            done();
        })
        .catch((err) => { 
            console.log(err.message)
            done();
        })
    });

    it('Updating the Data to db', (done) => {
        request(apprest)
        .post('/update')
        .set('Accept', 'Application/json') 
        .send({
                "CustId": 20,
                "CustomerName": "Mick"
        })
        .then((res) => { 
            const body = res.body
            console.log(res);
            done();
        })
        .catch((err) => { 
            console.log(err.message)
            done();
        })
    });

    it('Deleting the Data from db', (done) => {
        request(apprest)
        .post('/remove')
        .set('Accept', 'Application/json') 
        .send({
                "CustId": 12,
                "CustomerName": "Mick"
        })
        .then((res) => { 
            const body = res.body
            console.log(res);
            done();
        })
        .catch((err) => { 
            console.log(err.message)
            done();
        })
    });
});

