const oracledb = require('oracledb');
const express = require('express');

const app = express();
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 5000;

app.use(bodyParser.json()); //for parsing the incoming msg application.json\

app.use(bodyParser.urlencoded({ extended: true}));

// OracleDb Format
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
oracledb.autoCommit = true;

// Getting all the data From DB
app.get('/show',async(req,res)=>{
    let connection = null
    try{
        let connection = await oracledb.getConnection({
            user: "kv11",
            password: "kv_credence",
            connectString: "192.168.1.94:1521/srorcl"
        });
        const result = await connection.execute(
                        `SELECT * FROM customer_details`
                    );
                    console.log(result.rows)
                   return  res.json(result.rows)
    }
    catch(err){
        console.log(err)
        return res.send("Error")
    }
    finally{
        if(connection){
            try{
                await connection.close()
            }
            catch(err){
                console.log("Finally error")
                console.log(err)
            }
        }
    }
})


// Creating db Posting into Table

//CREATE 
app.post('/send', async (req, res) => {
    // console.log(req.body)
    let connection = null
    try {
        let connection = await oracledb.getConnection({
            user: "kv11",
            password: "kv_credence",
            connectString: "192.168.1.94:1521/srorcl"
        });
        const insert_q = `INSERT INTO customer_details (CustId, CustomerName, Address, City, PostalCode) values (:CustId, :CustomerName, :Address, :City, :PostalCode)`;
        var CustId = req.body.CustId;
        var CustomerName = req.body.CustomerName;
        var Address = req.body.Address;
        var City = req.body.City;
        var PostalCode  = req.body.PostalCode;
        let bind = []
        
        bind.push(CustId, CustomerName, Address, City, PostalCode);
      
        var binds = [];

        binds.push(bind);

        insert = await connection.executeMany(insert_q, binds)
        //console.log(binds)
        return res.send(binds)
    }
    catch (err) {
        console.log(err)
        return res.send("Error")
    }
    finally{
        if(connection){
            try{
                await connection.close()
            }
            catch(err){
                console.log("finally error")
                console.log(err)
            }
        }
    }  
});


// updating the DB

app.post('/update',async (req,res)=>{
    let connection = null
    try{
        connection = await oracledb.getConnection({
            user: "kv11",
            password: "kv_credence",
            connectString: "192.168.1.94:1521/srorcl"
        });

        const update = `UPDATE Customer_details SET CustID = :CustId WHERE CustomerName = :CustomerName`;
        const result = await connection.execute(update,{CustomerName: req.body.CustomerName, CustId: req.body.CustId})
        console.log(JSON.stringify(result));
        res.json(result)
    }
    
    catch(err){
    
        console.log(err)
    
    }
    
    finally{
        if(connection){
            try{
                await connection.close()
            }
            catch(err){
                console.log("Finally error")
                console.log(err)
            }
        }
    }

})


// Deleting the entries from DB

app.post('/remove',async(req,res)=>{
    let connection = null
    try{
        connection = await oracledb.getConnection({
            user: "kv11",
            password: "kv_credence",
            connectString: "192.168.1.94:1521/srorcl"
        });
  
        const del = `DELETE FROM customer_details WHERE CustomerName = :CustomerName`;
        const result = await connection.execute(del, {CustomerName: req.body.CustomerName});
        console.log(JSON.stringify(result));
        res.json(result)
  
    }
    catch(err){
        console.log(err)
    }
    finally{
        if(connection){
            try{
                await connection.close()
            }
            catch(err){
                console.log("Finally error");
                console.log(err);
            }
        }
    }
  })
  
//   app.listen(3300, () => console.log("Server started"))
app.listen(PORT, () => console.log(`server started on port ${PORT}`));
